if exists('loaded_redir')
  finish
endif
let loaded_redir = 1

command! -nargs=+ -complete=command Redir let s:reg = @@ | redir @"> | silent execute <q-args> | redir END | new | pu | 0,2d_ | let @@ = s:reg

